#include "fft.h"

void Editor::start(char* file){
    std::string in(file);
    in = "input\\" + in;
    image = cimg_library::CImg<double>(in.c_str());
    //image.channel(0);
    convert1();
    while(running){
        std::cout << "Select operation, 0 - save and finish, 1 - FFT"<<std::endl;
        std::cin >> select;

        switch(select){
            case 0:
                finish(file);
                break;
            case 1:
                fft();
                break;
        }
    }
}

//from 0 to 255 to 0 to 1
void Editor::convert1(){
    for (int c = 0; c < 3; c++){
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                image(i,j,0,c) /= 255 ;
            }
        }
    }
}

//from 0 to 1 to 0 to 255
void Editor::convert255(){
    for (int c = 0; c < 3; c++){
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                image(i,j,0,c) = std::round(image(i,j,0,c) * 255);
            }
        }
    }
}

//save the image and exit the program
void Editor::finish(char* file){
    convert255();
    std::string out(file);
    out.erase(out.length()-4);
    out ="output\\" + out + "edited.bmp";
    image.save(out.c_str());
    running = false;
}

void Editor::fft(){
    //grayscale image only
    image.channel(0);
    fftw_complex * in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * image.width() * image.height());
    //copy image data to input file 2D to 1D row major array (complex with i = 0)
    int cnt = 0;
    for (int i = 0; i < image.height(); ++i) {
        for (int j = 0; j < image.width(); ++j) {
            in[cnt][0] = image(j,i,0,0);
            in[cnt][1] = 0.0;
            cnt++;
        }
    }
    fftw_complex * out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * image.width() * image.height());
    fftw_plan p = fftw_plan_dft_2d(image.height(), image.width(), in, out, FFTW_FORWARD, FFTW_ESTIMATE);

    fftw_execute(p);
    fftw_destroy_plan(p);
    fftw_free(in);

    //copy output data back into image object (logarithm of absolute value of complex numbers), shift quadrants to have lowest freqencies in the middle
    cnt = 0;
    double max = 0;
    double second = 0;
    double min = 9999999;

    int offsetx = (image.width())/2;
    int offsety = (image.height())/2;
    
    for (int i = 0; i < offsety; ++i){
        //top left -> bottom right
        for (int j = 0; j < offsetx; ++j){
            image(j + offsetx,i+offsety,0,0) = log(1 + sqrt(out[cnt][0]*out[cnt][0] + out[cnt][1]*out[cnt][1]));
            cnt++;
        }
        //top right -> bottom left
        for (int j = 0; j < offsetx; ++j){
            image(j,i+offsety,0,0) = log(1 + sqrt(out[cnt][0]*out[cnt][0] + out[cnt][1]*out[cnt][1]));
            cnt++;
        }
    }
    
    for (int i = 0; i < offsety; ++i){
        //bottom left -> top right
        for (int j = 0; j < offsetx; ++j){
            image(j+offsetx,i,0,0) = log(1 + sqrt(out[cnt][0]*out[cnt][0] + out[cnt][1]*out[cnt][1]));
            cnt++;
        }
        //bottom right -> top left
        for (int j = 0; j < offsetx; ++j){
            image(j,i,0,0) = log(1 + sqrt(out[cnt][0]*out[cnt][0] + out[cnt][1]*out[cnt][1]));
            cnt++;
        }
    }
    //find min max for normalization
    for (int i = 0; i < image.height(); ++i) {
        for (int j = 0; j < image.width(); ++j) {
            if (image(j,i,0,0) > max){
                second = max;
                max = image(j,i,0,0);
            }
            if (image(j,i,0,0) < min){
                min = image(j,i,0,0);
            }
        }
    }

    fftw_free(out);

    double range = max - min;

    std::cout << max <<" "<<second<<std::endl;

    //normalize output to 0...1 range
    for (int i = 0; i < image.height(); ++i) {
        for (int j = 0; j < image.width(); ++j) {
            image(j,i,0,0) = (image(j,i,0,0) - min)/range;
        }
    }
    //add two more channels for other operations to work
    image.resize(image.width(),image.height(),1,3);
}

int main(int argc, char **argv) {
    if (argc > 1){
        Editor e;
	    e.start(argv[1]);
    }
    return 0;
}