#include "monadic.h"

void Editor::start(char* file){
    image = cimg_library::CImg<double>(file);
    //image.channel(0);
    convert1();
    while(running){
        std::cout << "Select operation, 0 - save and finish, 1 - brightness, 2 - contrast, 3 - equalize"<<std::endl;
        std::cin >> select;

        switch(select){
            case 0:
                finish(file);
                break;
            case 1:
                brightness();
                break;
            case 2:
                contrast();
                break;
            case 3:
                equalize();
                break;
        }
    }
}

//from 0 to 255 to 0 to 1
void Editor::convert1(){
    for (int c = 0; c < 3; c++){
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                image(i,j,0,c) /= 255 ;
            }
        }
    }
}

//from 0 to 1 to 0 to 255
void Editor::convert255(){
    for (int c = 0; c < 3; c++){
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                image(i,j,0,c) = std::round(image(i,j,0,c) * 255);
            }
        }
    }
}

//save the image and exit the program
void Editor::finish(char* file){
    convert255();
    std::string out(file);
    out.erase(out.length()-4);
    out += "edited.bmp";
    image.save(out.c_str());
    running = false;
}

//for each channel add value to each pixel, clamp if necessary
void Editor::brightness(){
    double value;
    std::cout << "Enter value"<<std::endl;
    std::cin >> value;
    for (int c = 0; c < 3; c++){
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                image(i,j,0,c) += value;
                if (image(i,j,0,c) > 1.0){
                    image(i,j,0,c) = 1.0;
                } else if (image(i,j,0,c) < 0.0) {
                    image(i,j,0,c) = 0.0;
                }
            }
        }
    }
}

//for each channel multiply value of each pixel, clamp if necessary
void Editor::contrast(){
    double value;
    std::cout << "Enter value"<<std::endl;
    std::cin >> value;
    for (int c = 0; c < 3; c++){
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                image(i,j,0,c) *= value;
                if (image(i,j,0,c) > 1.0){
                    image(i,j,0,c) = 1.0;
                } else if (image(i,j,0,0) < 0.0) {
                    image(i,j,0,c) = 0.0;
                }
            }
        }
    }
}

//equalize histogram of the image
void Editor::equalize(){
    convert255();
    for (int c = 0; c < 3; c++){
        std::vector<double> pdf(256);
        std::vector<double> cdf(256);
        //get pdf (histogram)
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                pdf[(int)image(i,j,0,c)] += 1.0;
            }
        }
        //calculate cdf
        double val = 0;
        double cdfmin = 0;
        for (int i = 0; i < 256; i++){
            val += pdf[i];
            if (cdfmin == 0 && val != 0){
                cdfmin = val;
                std::cout << cdfmin << std::endl;
            }
            cdf[i] = val;
        }
        //normalize cdf values (0,1)
        for (int i = 0; i < 256; i++){
            cdf[i] = (cdf[i] - cdfmin)/(val - cdfmin);
        }
        //equalize pixels
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                image(i,j,0,c) = cdf[int(image(i,j,0,c))];
            }
        }
    }
}

int main(int argc, char **argv) {
    if (argc > 1){
        Editor e;
	    e.start(argv[1]);
    }
    return 0;
}