#include "convolve.h"

void Editor::start(char* file){
    std::string in(file);
    in = "input\\" + in;
    image = cimg_library::CImg<double>(in.c_str());
    //image.channel(0);
    convert1();
    while(running){
        std::cout << "Select operation, 0 - save and finish, 1 - convolve, 2 - convolve separable"<<std::endl;
        std::cin >> select;
        switch(select){
            case 0:
                finish(file);
                break;
            case 1:
                {
                float sigma;
                std::cout << "Enter sigma"<<std::endl;
                std::cin >> sigma;
                
                std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
                convolve(sigma);
                std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
                
                //save measured time to file
                std::ofstream f;
                f.open("output\\measure.txt", std::ios_base::app);
                f <<"Convolution2D - Sigma: "<<sigma<<" time: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() <<" milliseconds"<<std::endl;
                f.close();
                break;
                }
            case 2:
                {
                float sigma;
                std::cout << "Enter sigma"<<std::endl;
                std::cin >> sigma;
                
                std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
                convolveSeparable(sigma);
                std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
                
                //save measured time to file
                std::ofstream f;
                f.open("output\\measure.txt", std::ios_base::app);
                f <<"ConvolutionSeparable - Sigma: "<<sigma<<" time: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() <<" milliseconds"<<std::endl;
                f.close();
                break;
                }
        }
    }
}

//from 0 to 255 to 0 to 1
void Editor::convert1(){
    for (int c = 0; c < 3; c++){
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                image(i,j,0,c) /= 255 ;
            }
        }
    }
}

//from 0 to 1 to 0 to 255
void Editor::convert255(){
    for (int c = 0; c < 3; c++){
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                image(i,j,0,c) = std::round(image(i,j,0,c) * 255);
            }
        }
    }
}

//save the image and exit the program
void Editor::finish(char* file){
    convert255();
    std::string out(file);
    out.erase(out.length()-4);
    out ="output\\" + out + "edited.bmp";
    image.save(out.c_str());
    running = false;
}

std::vector<std::vector<float>> kernel2D(float sigma){
    std::vector<std::vector<float>> kernel;
    int dim = 2 * floor(3*sigma) + 1;
    float variance2 = sigma*sigma*2;
    float sum = 0;
    kernel.resize(dim);
    for (auto &r : kernel){
        r.resize(dim);
    }
    int side = dim/2;

    //fill kernel with gaussian distribution
    for (int i = -side; i <= side; ++i){
        for(int j = -side; j <= side; ++j){
            kernel[i + side][j + side] = exp(-(i*i + j*j)/variance2);
            sum += kernel[i + side][j + side];
        }
    }

    //normalize kernel
    for (auto &r: kernel){
        for (auto &val: r){
            val /= sum;
        }
    }

    return kernel;
}

std::vector<float> kernel1D(float sigma){
    std::vector<float> kernel;
    int dim = 2 * floor(3*sigma) + 1;
    float variance2 = sigma*sigma*2;
    float sum = 0;
    kernel.resize(dim);
    int side = dim/2;
    //fill kernel with gaussian distribution
    for (int i = -side; i <= side; ++i){
        kernel[i + side] = exp(-(i*i) / variance2);
        sum += kernel[i + side];
    }

    //normalize kernel
    for (auto &val: kernel){
        val /= sum;
    }
    

    return kernel;
}

void Editor::convolve(float sigma){
    //grayscale image only
    image.channel(0);

    std::vector<std::vector<float>> kernel = kernel2D(sigma);

    cimg_library::CImg<double> imageNew = image;

    int side = kernel.size()/2;

    //iterate over all pixels
    for (int i = 0; i < image.width(); ++i){
        for(int j = 0; j < image.height(); ++j){
            //iterate over neighborhood
            float cur = 0;
            int x_c, y_c;
            for(int x = -side; x <= side; ++x){
                for(int y = -side; y <= side; ++y){
                    x_c = i + x;
                    y_c = j + y;
                    //out of bounds check
                    if (x_c < 0){
                        x_c = 0;
                    }else if(x_c >= image.width() ){
                        x_c = image.width() - 1;
                    }
                    if (y_c < 0){
                        y_c = 0;
                    }else if(y_c >= image.height() ){
                        y_c = image.height() - 1;
                    }
                    cur += kernel[x + side][y + side] * image(x_c, y_c, 0, 0);

                }
            }
            imageNew(i, j, 0, 0) = cur;
        }
    }

    image = imageNew;
    //add two more channels for other operations to work
    image.resize(image.width(),image.height(),1,3);
}

void Editor::convolveSeparable(float sigma){
        //grayscale image only
    image.channel(0);

    std::vector<float> kernel = kernel1D(sigma);

    cimg_library::CImg<double> imageNew = image;

    int side = kernel.size()/2;

    //iterate over all pixels horizontal kernel
    for (int i = 0; i < image.width(); ++i){
        for(int j = 0; j < image.height(); ++j){
            //iterate over neighborhood
            float cur = 0;
            int x_c;
            for(int x = -side; x <= side; ++x){
                x_c = i + x;
                if (x_c < 0){
                    x_c = 0;
                }else if(x_c >= image.width() ){
                    x_c = image.width() - 1;
                }
                
                cur += kernel[x + side] * image(x_c, j, 0, 0);

            }
            imageNew(i, j, 0, 0) = cur;
        }
    }
    //iterate over all pixels vertical kernel
    for (int i = 0; i < image.width(); ++i){
        for(int j = 0; j < image.height(); ++j){
            //iterate over neighborhood
            float cur = 0;
            int y_c;
            for(int y = -side; y <= side; ++y){
                y_c = j + y;
                if (y_c < 0){
                    y_c = 0;
                }else if(y_c >= image.height() ){
                    y_c = image.height() - 1;
                }
                
                cur += kernel[y + side] * imageNew(i, y_c, 0, 0);

            }
            image(i, j, 0, 0) = cur;
        }
    }
    //add two more channels for other operations to work
    image.resize(image.width(),image.height(),1,3);
}

int main(int argc, char **argv) {
    if (argc > 1){
        Editor e;
	    e.start(argv[1]);
    }
    return 0;
}