#include "stitch.h"

void Editor::start(char* file1, char* file2){
    std::string in1(file1);
    std::string in2(file2);
    in1 = "input\\" + in1;
    in2 = "input\\" + in2;
    image1 = cimg_library::CImg<float>(in1.c_str());
    image2 = cimg_library::CImg<float>(in2.c_str());
    //image.channel(0);
    convert1(image1);
    convert1(image2);
    while(running){
        std::cout << "Select operation, 0 - save and finish, 1 - stitch, 2 - stitch simple "<<std::endl;
        std::cin >> select;
        switch(select){
            case 0:
                finish(file1);
                break;
            case 1:
                {
                stitch();
                break;
                }
            case 2:
                {
                stitchSimple();
                break;
                }
        }
    }
}

void normalize(cimg_library::CImg<float> &image){
    float max = 0;
    float min = 9999999;
    for (int i = 0; i < image.width(); ++i){
        for(int j = 0; j < image.height(); ++j){
            if (image(i,j,0,0) < min){
                min = image(i,j,0,0);
            }
            if (image(i,j,0,0) > max){
                max = image(i,j,0,0);
            }
        }
    }
    float range = max - min;
    for (int i = 0; i < image.width(); ++i){
        for(int j = 0; j < image.height(); ++j){
            image(i,j,0,0) = (image(i,j,0,0) - min)/range;
        }
    }
}

void clamp(cimg_library::CImg<float> &image){
    for (int i = 0; i < image.width(); ++i){
        for(int j = 0; j < image.height(); ++j){
            if (image(i,j,0,0) > 1.0){
                image(i,j,0,0) = 1.0;
            }else if(image(i,j,0,0) < 0.0){
                image(i,j,0,0) = 0.0;
            }
        }
    }
}

void Editor::stitchSimple(){
    image1.channel(0);
    image2.channel(0);
    imageOut = image1;
    //merge images directly
    for (int i = image2.width()/2; i < image2.width(); ++i){
        for (int j = 0; j < image2.height(); ++j){
            imageOut(i,j,0,0) = image2(i,j,0,0);
        }
    }
}

void Editor::stitch(){
    image1.channel(0);
    image2.channel(0);
    //sobel kernel
    //std::vector<std::vector<float>> kernelX{{1,0,-1},{2,0,-2},{1,0,-1}};
    //std::vector<std::vector<float>> kernelY{{1,2,1},{0,0,0},{-1,-2,-1}};

    //difference kernel
    std::vector<std::vector<float>> kernelX{{0,0,0},{0,-1,1},{0,0,0}};
    std::vector<std::vector<float>> kernelY{{0,0,0},{0,-1,0},{0,1,0}};

    auto gradX1 = convolve(kernelX, image1);
    auto gradX2 = convolve(kernelX, image2);
    auto gradY1 = convolve(kernelY, image1);
    auto gradY2 = convolve(kernelY, image2);

    auto gradX12 = gradX1;
    auto gradY12 = gradY1;

    //merge gradient fields
    for (int i = image2.width()/2; i < image2.width(); ++i){
        for (int j = 0; j < image2.height(); ++j){
            gradX12(i,j,0,0) = gradX2(i,j,0,0);
            gradY12(i,j,0,0) = gradY2(i,j,0,0);
        }
    }

    //calculate divergence
    auto gradXX = convolve(kernelX, gradX12);
    auto gradYY = convolve(kernelY, gradY12);

    cimg_library::CImg<float> divergence = gradXX;
    for (int i = 0; i < image2.width(); ++i){
        for (int j = 0; j < image2.height(); ++j){
            divergence(i,j,0,0) += gradYY(i,j,0,0);
        }
    }
    //set initial image as stitched images in greyscale
    stitchSimple();

    float change = 100;
    while(change > 1){
        change = 0;
        for (int i = 1; i < imageOut.width()-1; ++i){
            for (int j = 1; j < imageOut.height()-1; ++j){
                auto oldVal = imageOut(i,j,0,0);
                imageOut(i,j,0,0) = (imageOut(i-1,j,0,0) + imageOut(i+1,j,0,0) + imageOut(i,j-1,0,0) + imageOut(i,j+1,0,0) - divergence(i,j,0,0))/4;
                change += std::abs(imageOut(i,j,0,0) - oldVal);  
            }
        }
        //edges x
        for (int i = 1; i < imageOut.width()-1; ++i){
            auto oldVal = imageOut(i,0,0,0);
            imageOut(i,0,0,0) = (imageOut(i-1,0,0,0) + imageOut(i+1,0,0,0) + imageOut(i,1,0,0) + image2(i,0,0,0) - divergence(i,0,0,0))/4;
            change += std::abs(imageOut(i,0,0,0) - oldVal);
            

            int j = imageOut.height()-1;
            oldVal = imageOut(i,j,0,0);
            imageOut(i,j,0,0) = (imageOut(i-1,j,0,0) + imageOut(i+1,j,0,0) + imageOut(i,j-1,0,0) + image2(i,j,0,0) - divergence(i,j,0,0))/4;
            change += std::abs(imageOut(i,j,0,0) - oldVal);
            
        }
        //edges y (left side first image right side second image values)
        for (int j = 1; j < imageOut.height()-1; ++j){
            auto oldVal = imageOut(0,j,0,0);
            imageOut(0,j,0,0) = (imageOut(0,j-1,0,0) + imageOut(0,j+1,0,0) + imageOut(1,j,0,0) + image2(0,j,0,0) - divergence(0,j,0,0))/4;
            change += std::abs(imageOut(0,j,0,0) - oldVal);
            
            int i = imageOut.width()-1;
            oldVal = imageOut(i,j,0,0);
            imageOut(i,j,0,0) = (imageOut(i-1,j,0,0) + imageOut(i,j-1,0,0) + imageOut(i,j+1,0,0) + image2(i,j,0,0) - divergence(i,j,0,0))/4;
            change += std::abs(imageOut(i,j,0,0) - oldVal);
            
        }
        //corners
        //0,0
        auto oldVal = imageOut(0,0,0,0);
        imageOut(0,0,0,0) = (imageOut(1,0,0,0) + imageOut(0,1,0,0) + image2(0,0,0,0) + image2(0,0,0,0) - divergence(0,0,0,0))/4;
        change += std::abs(imageOut(0,0,0,0) - oldVal);
        
        //0,height
        int j = imageOut.height();
        oldVal = imageOut(0,j,0,0);
        imageOut(0,j,0,0) = (imageOut(1,j,0,0) + imageOut(0,j-1,0,0) + image2(0,j,0,0) + image2(0,j,0,0) - divergence(0,j,0,0))/4;
        change += std::abs(imageOut(0,j,0,0) - oldVal);
        
        //width,height
        int i = imageOut.width();
        oldVal = imageOut(i,j,0,0);
        imageOut(i,j,0,0) = (imageOut(i-1,j,0,0) + imageOut(i,j-1,0,0) + image2(i,j,0,0) + image2(i,j,0,0) - divergence(i,j,0,0))/4;
        change += std::abs(imageOut(i,j,0,0) - oldVal);
        
        //width,0
        oldVal = imageOut(i,0,0,0);
        imageOut(i,0,0,0) = (imageOut(i,1,0,0) + imageOut(i-1,0,0,0) + image2(i,0,0,0) + image2(i,0,0,0) - divergence(i,0,0,0))/4;
        change += std::abs(imageOut(i,0,0,0) - oldVal);
        
        std::cout << change <<std::endl;
    }
    clamp(imageOut);
}

//from 0 to 255 to 0 to 1
void Editor::convert1(cimg_library::CImg<float> &image){
    for (int c = 0; c < image.spectrum(); c++){
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                image(i,j,0,c) /= 255 ;
            }
        }
    }
}

//from 0 to 1 to 0 to 255
void Editor::convert255(cimg_library::CImg<float> &image){
    for (int c = 0; c < image.spectrum(); c++){
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                image(i,j,0,c) = std::round(image(i,j,0,c) * 255);
            }
        }
    }
}

//save the image and exit the program
void Editor::finish(char* file){
    convert255(imageOut);
    std::string out(file);
    out.erase(out.length()-4);
    out ="output\\" + out + "edited.bmp";
    imageOut.save(out.c_str());
    running = false;
}



cimg_library::CImg<float> Editor::convolve(std::vector<std::vector<float>> kernel, cimg_library::CImg<float> &image){
    //grayscale image only
    image.channel(0);

    cimg_library::CImg<float> imageNew = image;

    int side = kernel.size()/2;

    //iterate over all pixels
    for (int i = 0; i < image.width(); ++i){
        for(int j = 0; j < image.height(); ++j){
            //iterate over neighborhood
            float cur = 0;
            int x_c, y_c;
            for(int x = -side; x <= side; ++x){
                for(int y = -side; y <= side; ++y){
                    x_c = i + x;
                    y_c = j + y;
                    //out of bounds check
                    if (x_c < 0){
                        x_c = 0;
                    }else if(x_c >= image.width() ){
                        x_c = image.width() - 1;
                    }
                    if (y_c < 0){
                        y_c = 0;
                    }else if(y_c >= image.height() ){
                        y_c = image.height() - 1;
                    }
                    cur += kernel[x + side][y + side] * image(x_c, y_c, 0, 0);

                }
            }
            imageNew(i, j, 0, 0) = cur;
        }
    }

    return imageNew;
}


int main(int argc, char **argv) {
    if (argc > 1){
        Editor e;
	    e.start(argv[1], argv[2]);
    }
    return 0;
}