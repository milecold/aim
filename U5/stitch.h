#define cimg_display 0
#include "CImg\CImg.h"
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <fstream>
#include <chrono>
#include <cstdlib>
#include <ctime>

class Editor {
	public:
	void start(char* file1, char* file2);

	private:
	int select;
	bool running = true;
	cimg_library::CImg<float> imageOut;
	cimg_library::CImg<float> image1;
	cimg_library::CImg<float> image2;
	void convert1(cimg_library::CImg<float> &image);
	void convert255(cimg_library::CImg<float> &image);
	void finish(char* file);
	void stitch();
	void stitchSimple();
	cimg_library::CImg<float> convolve(std::vector<std::vector<float>> kernel, cimg_library::CImg<float> &image);
};







