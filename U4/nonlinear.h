#define cimg_display 0
#include "CImg\CImg.h"
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <fstream>
#include <chrono>
#include <cstdlib>
#include <ctime>

class Editor {
	public:
	void start(char* file);

	private:
	int select;
	bool running = true;
	cimg_library::CImg<double> image;
	void convert1();
	void convert255();
	void finish(char* file);
	void convolveNonLinear(float sigmaB, float sigmaS);
	void addNoise(float noise);
};







