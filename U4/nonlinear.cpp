#include "nonlinear.h"

void Editor::start(char* file){
    std::string in(file);
    in = "input\\" + in;
    image = cimg_library::CImg<double>(in.c_str());
    //image.channel(0);
    convert1();
    while(running){
        std::cout << "Select operation, 0 - save and finish, 1 - convolve non linear, 2 - add noise"<<std::endl;
        std::cin >> select;
        switch(select){
            case 0:
                finish(file);
                break;
            case 1:
                {
                float sigmaR, sigmaS;
                std::cout << "Enter sigma (space)"<<std::endl;
                std::cin >> sigmaS;
                std::cout << "Enter sigma (range)"<<std::endl;
                std::cin >> sigmaR;
                convolveNonLinear(sigmaS, sigmaR);
               
                break;
                }
            case 2:
                {
                float noise;
                std::cout << "Enter noise amount"<<std::endl;
                std::cin >> noise;
                addNoise(noise);
                }
        }
    }
}

//from 0 to 255 to 0 to 1
void Editor::convert1(){
    for (int c = 0; c < 3; c++){
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                image(i,j,0,c) /= 255 ;
            }
        }
    }
}

//from 0 to 1 to 0 to 255
void Editor::convert255(){
    for (int c = 0; c < 3; c++){
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                image(i,j,0,c) = std::round(image(i,j,0,c) * 255);
            }
        }
    }
}

//save the image and exit the program
void Editor::finish(char* file){
    convert255();
    std::string out(file);
    out.erase(out.length()-4);
    out ="output\\" + out + "edited.bmp";
    image.save(out.c_str());
    running = false;
}

std::vector<std::vector<float>> kernel2D(float sigma){
    std::vector<std::vector<float>> kernel;
    int dim = 2 * floor(3*sigma) + 1;
    float variance2 = sigma*sigma*2;
    float sum = 0;
    kernel.resize(dim);
    for (auto &r : kernel){
        r.resize(dim);
    }
    int side = dim/2;

    //fill kernel with gaussian distribution
    for (int i = -side; i <= side; ++i){
        for(int j = -side; j <= side; ++j){
            kernel[i + side][j + side] = exp(-(i*i + j*j)/variance2);
            sum += kernel[i + side][j + side];
        }
    }
    
    return kernel;
}

float gauss1d(float variance2, float x){


    return exp(-(x*x) / variance2);

}

void Editor::addNoise(float noise){
    //random seed
    srand((unsigned) time(0));
    image.channel(0);
    for (int i = 0; i < image.width(); ++i){
        for(int j = 0; j < image.height(); ++j){
            
            image(i,j,0,0) = image(i,j,0,0) + (2*noise*((float)rand() / RAND_MAX) -noise);
            if (image(i,j,0,0) < 0){
                image(i,j,0,0) = 0;
            }else if(image(i,j,0,0) > 1){
                image(i,j,0,0) = 1;
            }
        }
    }
    image.resize(image.width(),image.height(),1,3);
}

void Editor::convolveNonLinear(float sigmaS, float sigmaR){
    //grayscale image only
    image.channel(0);

    std::vector<std::vector<float>> kernel = kernel2D(sigmaS);

    float variance2 = sigmaR * sigmaR * 2;

    cimg_library::CImg<double> imageNew = image;

    int side = kernel.size()/2;

    //iterate over all pixels
    for (int i = 0; i < image.width(); ++i){
        for(int j = 0; j < image.height(); ++j){
            //iterate over neighborhood
            float cur = 0;
            float w = 0;
            float Fxy = image(i, j, 0, 0);
            int x_c, y_c;
            for(int x = -side; x <= side; ++x){
                for(int y = -side; y <= side; ++y){
                    x_c = i + x;
                    y_c = j + y;
                    //out of bounds check
                    if (x_c < 0){
                        x_c = 0;
                    }else if(x_c >= image.width() ){
                        x_c = image.width() - 1;
                    }
                    if (y_c < 0){
                        y_c = 0;
                    }else if(y_c >= image.height() ){
                        y_c = image.height() - 1;
                    }
                    float bxG =  kernel[x + side][y + side] * gauss1d(variance2, Fxy - image(x_c, y_c, 0, 0));
                    w += bxG;
                    cur += bxG * image(x_c, y_c, 0, 0);
                    
                }
            }
            //save weighed new pixel value
            imageNew(i, j, 0, 0) = cur/w;
        }
    }

    image = imageNew;
    //add two more channels for other operations to work
    image.resize(image.width(),image.height(),1,3);
}


int main(int argc, char **argv) {
    if (argc > 1){
        Editor e;
	    e.start(argv[1]);
    }
    return 0;
}