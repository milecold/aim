#define cimg_display 0
#include "CImg\CImg.h"
#include "GridGraph_2D_4C.h"
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <fstream>
#include <chrono>
#include <cstdlib>
#include <ctime>

class Editor {
	public:
	void start(char* file1);

	private:
	int select;
	bool running = true;
	cimg_library::CImg<float> imageOut;
	cimg_library::CImg<float> image1;
	cimg_library::CImg<float> image2;
	void convert1(cimg_library::CImg<float> &image);
	void convert255(cimg_library::CImg<float> &image);
	void finish(char* file);
	void save(char* file);
	void colorDrawing();
	void colorDrawing(std::vector<std::vector<int>> first, std::vector<std::vector<int>> second);
	void fillInput(const cimg_library::CImg<float> &image, const std::vector<std::vector<int>> &first, const std::vector<std::vector<int>> &second);
};







