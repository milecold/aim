#include "color.h"

void Editor::start(char* file1){
    std::string in1(file1);
    in1 = "input\\" + in1;
    image1 = cimg_library::CImg<float>(in1.c_str());
    imageOut = image1;

    while(running){
        std::cout << "Select operation, 0 - save and finish, 1 - color_manual (2 colors), 2 - color from image "<<std::endl;
        std::cin >> select;
        switch(select){
            case 0:
                finish(file1);
                break;
            case 1:
                {
                std::vector<std::vector<int>> first(2);
                std::vector<std::vector<int>> second(2);

                for (int i = 0; i < 2; ++i){
                    first[i].resize(5);
                    second[i].resize(5);
                    std::cout << "First position and color: x y r g b"<<std::endl;
                    std::cin >> first[i][0] >> first[i][1] >> first[i][2] >> first[i][3] >> first[i][4];
                    std::cout << "Second position and color: x y r g b"<<std::endl;
                    std::cin >> second[i][0] >> second[i][1] >> second[i][2] >> second[i][3] >> second[i][4];
                }

                fillInput(image1, first, second);
                save(file1);
                colorDrawing(first, second);
                break;
                }
            case 2:
                std::string in2;
                std::cin >> in2;
                in2 = "input\\" + in2;
                image2 = cimg_library::CImg<float>(in2.c_str());
                colorDrawing();
        }
    }
}

void normalize(cimg_library::CImg<float> &image){
    float max = 0;
    float min = 9999999;
    for (int i = 0; i < image.width(); ++i){
        for(int j = 0; j < image.height(); ++j){
            if (image(i,j,0,0) < min){
                min = image(i,j,0,0);
            }
            if (image(i,j,0,0) > max){
                max = image(i,j,0,0);
            }
        }
    }
    float range = max - min;
    for (int i = 0; i < image.width(); ++i){
        for(int j = 0; j < image.height(); ++j){
            image(i,j,0,0) = (image(i,j,0,0) - min)/range;
        }
    }
}

void clamp(cimg_library::CImg<float> &image){
    for (int i = 0; i < image.width(); ++i){
        for(int j = 0; j < image.height(); ++j){
            if (image(i,j,0,0) > 1.0){
                image(i,j,0,0) = 1.0;
            }else if(image(i,j,0,0) < 0.0){
                image(i,j,0,0) = 0.0;
            }
        }
    }
}

void Editor::fillInput(const cimg_library::CImg<float> &image, const std::vector<std::vector<int>> &first, const std::vector<std::vector<int>> &second){
    imageOut = image;
    int rad = 50;
    for (int p = 0; p < first.size(); ++p){
        for (int c = 0; c < 3; ++c){
            for (int i = -rad; i <= rad; ++i ){
                for(int j = -rad; j <= rad; ++j){
                    imageOut(first[p][0] + i,first[p][1] + j,0,c) = first[p][c+2];
                }
            }
        }
        for (int c = 0; c < 3; ++c){
            for (int i = -rad; i <= rad; ++i ){
                for(int j = -rad; j <= rad; ++j){
                    imageOut(second[p][0] + i,second[p][1] + j,0,c) = second[p][c+2];
                }
            }
        }
    }
}

bool isNew(const std::vector<std::array<int,3>> &colors, const std::array<int,3> &color){
    //ignore black pixels


    if (color == std::array<int,3>{0,0,0}){
        return false;
    }
    for (auto c : colors){
        if (c == color){
            return false;
        }
    }
    return true;
}

std::vector<std::array<int,3>> getColors(const cimg_library::CImg<float> &image){
    std::vector<std::array<int,3>> colors;
    //finds colors in image except for white
    std::array<int,3> color;
    for (int i = 0; i < image.width(); ++i){
        for(int j = 0; j < image.height(); ++j){
            for (int c = 0; c < 3; ++c){
                color[c] = image(i,j,0,c);
            }
            if (isNew(colors, color)){
                std::cout << "new color" <<std::endl;
                std::cout << color[0] << " "<<color[1]<<" "<<color[2]<<std::endl;
                colors.push_back(color);
            }
        }
    }
    return colors;
}

void deleteImage(cimg_library::CImg<float> &image){
    for (int i = 0; i < image.width(); ++i){
        for(int j = 0; j < image.height(); ++j){
            for(int c = 0; c < 3; ++c){
                image(i,j,0,c) = -1;
            }
        }
    }
}

void deleteColor(cimg_library::CImg<float> &image, const std::array<int,3> &color){
    std::array<int,3> colorOld;
    for (int i = 0; i < image.width(); ++i){
        for(int j = 0; j < image.height(); ++j){
            for (int c = 0; c < 3; ++c){
                colorOld[c] = image(i,j,0,c);
            }
            if (colorOld == color){
                for (int c = 0; c < 3; ++c){
                    image(i,j,0,c) = 0;
                }
            }
        }
    }
}

//image input
void Editor::colorDrawing(){
    int K = 1000;
    int lK = 0.05 * K;

    deleteImage(imageOut);

    std::vector<std::array<int,3>> colors = getColors(image2);

    typedef GridGraph_2D_4C<int,int,int> Grid;
    image1.channel(0);
    std::array<int,3> pixel;
    Grid* grid;

    for (int c = 0; c < colors.size()-1;++c){

        grid = new Grid(image1.width(),image1.height());


        std::cout << c <<std::endl;
        for (int i = 0; i < image1.width(); ++i){
            for(int j = 0; j < image1.height(); ++j){
                
                if(imageOut(i,j,0,0) != -1){
                    continue;
                }

                for(int ch = 0; ch < 3; ++ch){
                    pixel[ch] = image2(i,j,0,ch);
                }
                //terminals
                if (pixel == colors[c]){
                    grid->set_terminal_cap(grid->node_id(i,j),lK,0);
                }else if(pixel == std::array<int,3>{0,0,0}){
                    grid->set_terminal_cap(grid->node_id(i,j),0,0);
                }else{
                    grid->set_terminal_cap(grid->node_id(i,j),0,lK);
                }
                    
                //neighbors
                if (i < image1.width()-1 && imageOut(i+1,j,0,0) == -1){
                    //x direction both ways
                    int w = (1 + pow(std::min(image1(i,j,0,0), image1(i+1,j,0,0)),1.2));
                    grid->set_neighbor_cap(grid->node_id(i,j),+1,0,w);
                    grid->set_neighbor_cap(grid->node_id(i+1,j),-1,0,w);
                }

                if (j < image1.height()-1 && imageOut(i,j+1,0,0) == -1){
                    //y direction both ways
                    int w = (1 + pow(std::min(image1(i,j,0,0), image1(i,j+1,0,0)),1.2));
                    grid->set_neighbor_cap(grid->node_id(i,j),0,+1,w);
                    grid->set_neighbor_cap(grid->node_id(i,j+1),0,-1,w);
                }
            }
        }


        grid->compute_maxflow();
        convert1(image1);

        //save output based on node in from gridcut
        for (int ch = 0; ch < 3; ++ch){
            for (int i = 0; i < image1.width(); ++i){
                for (int j = 0; j < image1.height(); ++j){
                    if (imageOut(i,j,0,ch) == -1 && grid->get_segment(grid->node_id(i,j)) == 0){
                        imageOut(i,j,0,ch) = image1(i,j) * colors[c][ch];
                    }
                }
            }
        }

        //deleteColor(image2, colors[c]);
        if(c == colors.size()-2){
            break;
        }
        
        convert255(image1);
        delete grid;
    }

    //last section
    for (int ch = 0; ch < 3; ++ch){
        for (int i = 0; i < image1.width(); ++i){
            for (int j = 0; j < image1.height(); ++j){
                if (imageOut(i,j,0,ch) == -1 && grid->get_segment(grid->node_id(i,j)) != 0){
                    imageOut(i,j,0,ch) = image1(i,j) * colors[colors.size()-1][ch];
                }
            }
        }
    }
    delete grid;
    
}

//console input
void Editor::colorDrawing(std::vector<std::vector<int>> first, std::vector<std::vector<int>> second){
    //first source, second sink
    int K = 1000;
    int lK = 0.05 * K;
    int rad = 50;

    typedef GridGraph_2D_4C<int,int,int> Grid;
    Grid* grid = new Grid(image1.width(),image1.height());
    image1.channel(0);
    for (int i = 0; i < image1.width(); ++i){
        for(int j = 0; j < image1.height(); ++j){
            //terminals
            bool term = false;
            for (int p = 0; p < first.size(); ++p){
                if (std::abs(i - first[p][0]) < rad && std::abs(j - first[p][1]) < rad){
                    grid->set_terminal_cap(grid->node_id(i,j),lK,0);
                    term = true;
                    break;
                }else if(std::abs(i - second[p][0]) < rad && std::abs(j - second[p][1]) < rad){
                    grid->set_terminal_cap(grid->node_id(i,j),0,lK);
                    term = true;
                    break;
                }
            }
            if (!term){
                grid->set_terminal_cap(grid->node_id(i,j),0,0);
            }

            //neighbors
            if (i < image1.width()-1){
                //x direction both ways
                int w = (1 + pow(std::min(image1(i,j,0,0), image1(i+1,j,0,0)),1.2));
                grid->set_neighbor_cap(grid->node_id(i,j),+1,0,w);
                grid->set_neighbor_cap(grid->node_id(i+1,j),-1,0,w);
            }

            if (j < image1.height()-1){
                //y direction both ways
                int w = (1 + pow(std::min(image1(i,j,0,0), image1(i,j+1,0,0)),1.2));
                grid->set_neighbor_cap(grid->node_id(i,j),0,+1,w);
                grid->set_neighbor_cap(grid->node_id(i,j+1),0,-1,w);
            }
        }
    }

    grid->compute_maxflow();
    convert1(image1);

    //save output based on node in from gridcut
    for (int c = 0; c < 3; ++c){
        for (int i = 0; i < image1.width(); ++i){
            for (int j = 0; j < image1.height(); ++j){
                if (grid->get_segment(grid->node_id(i,j)) == 0){
                    //std::cout << image1(i,j) * first[c+2] <<std::endl;
                    imageOut(i,j,0,c) = image1(i,j) * first[0][c+2];
                }else{
                    imageOut(i,j,0,c) = image1(i,j) * second[0][c+2];
                }
            }
        }
    }

    delete grid;
}


//from 0 to 255 to 0 to 1
void Editor::convert1(cimg_library::CImg<float> &image){
    for (int c = 0; c < image.spectrum(); c++){
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                image(i,j,0,c) /= 255 ;
            }
        }
    }
}

//from 0 to 1 to 0 to 255
void Editor::convert255(cimg_library::CImg<float> &image){
    for (int c = 0; c < image.spectrum(); c++){
        for (int i = 0; i < image.width(); ++i){
            for(int j = 0; j < image.height(); ++j){
                image(i,j,0,c) = std::round(image(i,j,0,c) * 255);
            }
        }
    }
}

void Editor::save(char* file){
    std::string out(file);
    out.erase(out.length()-4);
    out ="output\\" + out + "input.bmp";
    imageOut.save(out.c_str());
}

//save the image and exit the program
void Editor::finish(char* file){
    std::string out(file);
    out.erase(out.length()-4);
    out ="output\\" + out + "edited.bmp";
    imageOut.save(out.c_str());
    running = false;
}



int main(int argc, char **argv) {
    if (argc > 1){
        Editor e;
	    e.start(argv[1]);
    }
    return 0;
}